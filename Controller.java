package EPAMHome;

import java.util.Scanner;


public class Controller {

    static final String NAME_PARSER = "^[A-Z][a-z]+$";
    static final String PHONE_PARSER = "^(\\d{3}-){2}\\d{2}-\\d{2}$";
    static final String EMAIL_PARSER = "^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$";

    String[] allParser = {NAME_PARSER,PHONE_PARSER,EMAIL_PARSER};


    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public Model getModel() {
        return model;
    }

    public View getView() {
        return view;
    }
    public void startProgram(){
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < allParser.length; i++){
            System.out.println(view.returnMessage(view.goodMass[i]));
            String uservalue = scanner.nextLine();
            while (!model.checkUserData(uservalue,allParser[i])){
                System.out.println(view.returnMessage(view.bedMass[i]));
                uservalue = scanner.nextLine();
            }
            model.addToBase(uservalue);
        }
        System.out.println(View.DET_ALL_DATA);
        view.getAllInfo(model.getSimpleBase());
    }

}
