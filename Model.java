package EPAMHome;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Model {
    private ArrayList<String> simpleBase = new ArrayList<>();

    public boolean checkUserData(String userdata, String regul){
        Pattern pattern = Pattern.compile(regul);
        Matcher matcher = pattern.matcher(userdata);
        return matcher.find();
    }

    public void addToBase(String str){
        simpleBase.add(str);
    }

    public ArrayList<String> getSimpleBase(){
        return simpleBase;
    }

}
