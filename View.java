package EPAMHome;

import java.util.ArrayList;

public class View {
    static final String NAME_MESSAGE = "Введіть своє ім'я на англійській мові";
    static final String PHONE_MESSAGE = "Введіть свій номер телефону, як показано нижче \n" +
            "000-000-00-00";
    static final String EMAIL_MESSAGE = "Введіть адрес своєї електронної пошти";

    static final String ERROR_NAME_MESSAGE = "Перевірте коректність вводу свого імені";
    static final String ERROR_HONE_MESSAGE = "Перевірте коректність вводу свого номеу телефона";
    static final String ERROR_EMAIL_MESSAGE = "Перевірте коректність вводу адреу своєї електронної пошти";

    static final String DET_ALL_DATA = "Ваші данні";

    String[] goodMass = {NAME_MESSAGE,PHONE_MESSAGE,EMAIL_MESSAGE};
    String[] bedMass = {ERROR_NAME_MESSAGE,ERROR_HONE_MESSAGE,ERROR_EMAIL_MESSAGE};

    public String returnMessage(String message){
        return message;
    }
    public void getAllInfo(ArrayList<String> list){
        for (String str:list) {
            System.out.println(str);
        }
    }
}
